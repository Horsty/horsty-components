# Horsty Library

[![Netlify Status](https://api.netlify.com/api/v1/badges/b9642492-329f-4a7d-b263-4b9cb2688da0/deploy-status)](https://app.netlify.com/sites/youthful-edison-0295f4/deploys)

## SVG

### Add new svg

if you'r adding new svg, you need to run this cmd to transform svg into react component (for more info see this [Blog - Svgr](https://gregberge.com/fr/blog/svg-to-react-component-with-svgr) or [Git - gregberge/svgr](https://github.com/gregberge/svgr))

```
npm run build:svg
```

### Library componant naming #FunFact

Why `Holi` to prefixing component ? It is the contraction of Horsty and Library quite simply. It sound nice like `Holy`, and just use `Hl` wasn't readable enough

# Thanks for helping

- Tuto/Guide from HarveyD -> [Blog](https://blog.harveydelaney.com/creating-your-own-react-component-library/) / [Git](https://github.com/HarveyD/react-component-library)
- Tuto/Guide for Svg integration by gregberge -> [Blog](https://gregberge.com/fr/blog/)
- Very simple breakpoint approach -> [Article Medium](https://medium.com/codeartisan/breakpoints-and-media-queries-in-scss-46e8f551e2f2)
