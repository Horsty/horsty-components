module.exports = (componentName) => ({
  content: `// Generated with util/create-component.js
@import "../../gui";

.foo-bar {
  @include font-defaults;

  color: $blue;
}
`,
  extension: `.scss`
});
