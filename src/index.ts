/** Component */
import { default as HoliButton } from "./components/Button/Button";
import { default as HoliTextInput } from "./components/Input/Input";
import { default as HoliToast } from "./components/Toast/Toast";
import { default as HoliCard } from "./components/Card/Card";
import { default as HoliSvgLogo } from "./components/Icons/Logo";
import { default as HoliSvgCross } from "./components/Icons/Cross";
import { default as HoliSvgOrnement } from "./components/Icons/Ornement";
import { default as HoliSvgSearch } from "./components/Icons/Search";

export {
  HoliButton,
  HoliTextInput,
  HoliToast,
  HoliCard,
  HoliSvgLogo,
  HoliSvgCross,
  HoliSvgOrnement,
  HoliSvgSearch,
};
