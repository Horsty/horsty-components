import React from "react";
import "./Color.scss";
import PropTypes from "prop-types";

const Color = ({ type }) => {
  const hexaColor = getComputedStyle(document.documentElement).getPropertyValue(
    `--${type}`
  );
  return (
    <div className="colorBox">
      <div className={`color color--${type}`}>{hexaColor}</div>
      <p className="text">
        {type} : {hexaColor}
      </p>
    </div>
  );
};
Color.defaultProps = {
  type: "blue",
};
Color.propTypes = {
  type: PropTypes.string,
};

export default Color;
