import React from "react";
import SvgLogo from './Logo';
import SvgCross from './Cross';
import SvgOrnement from './Ornement';
import SvgSearch from './Search';

export default {
  title: "Available Icons",
  component: SvgLogo,
};

export const Logo = () => <SvgLogo width="100" height="50"/>;
export const Cross = () => <SvgCross  width="19" height="19"/>;
export const Ornement = () => <SvgOrnement/>;
export const Search = () => <SvgSearch/>;
