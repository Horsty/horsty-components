import * as React from "react";

function SvgSearch(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={19} height={19} {...props}>
      <path
        d="M15.134 14.126l3.42 3.42a.713.713 0 01-1.008 1.008l-3.42-3.42a.713.713 0 011.008-1.008zM7.79.238a7.552 7.552 0 110 15.104 7.552 7.552 0 010-15.104zm0 1.425a6.128 6.128 0 100 12.255 6.128 6.128 0 000-12.255zm-.57 1.662a.475.475 0 110 .95A2.945 2.945 0 004.275 7.22a.475.475 0 11-.95 0A3.895 3.895 0 017.22 3.325z"
        fillRule="nonzero"
      />
    </svg>
  );
}

export default SvgSearch;
