import * as React from "react";

function SvgOrnement(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={51} height={19} {...props}>
      <g fill="#F6C38A" fillRule="nonzero">
        <path d="M30.501 10l-8.464-8.543L13.572 10l8.465 8.543L30.5 10zm-8.465-7.123L29.092 10l-7.056 7.122L14.98 10l7.057-7.123z" />
        <path d="M38.428 10l-8.465-8.543L21.5 10l8.464 8.543L38.428 10zm-8.466-7.122L37.02 10l-7.057 7.123L22.906 10l7.056-7.122z" />
        <path d="M50.205 9v1h-13.41V9zM14.205 9v1H.795V9z" />
      </g>
    </svg>
  );
}

export default SvgOrnement;
