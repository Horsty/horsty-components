import * as React from "react";
import { action } from "@storybook/addon-actions";

import Button from "./Button";
export default {
  title: "Button",
  component: Button,
};
const actions = {
  onClick: action("onClick"),
};
let primaryButton = {
  text: "Selected",
  type: "primary",
};
/**
 * Primary button
 */
export const primary = () => (
  <Button text={primaryButton.text} type={primaryButton.type} {...actions} />
);
/**
 * Secondary button
 */
let secondaryButton = {
  text: "Enabled",
  type: "secondary",
};
export const secondary = () => (
  <Button
    text={secondaryButton.text}
    type={secondaryButton.type}
    {...actions}
  />
);

/**
 * Pressed button
 */
let pressedButton = {
  text: "Pressed",
  type: "pressed",
};
export const pressed = () => (
  <Button text={pressedButton.text} type={pressedButton.type} {...actions} />
);
