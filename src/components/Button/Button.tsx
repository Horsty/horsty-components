import React from "react";
import "./Button.scss";
import PropTypes from "prop-types";

const Button = ({ text, type, onClick }) => {
  return (
    <button className={`button button--${type}`} onClick={onClick}>
      {text}
    </button>
  );
};
Button.defaultProps = {
  text: "Create",
};
Button.propTypes = {
  text: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default Button;
