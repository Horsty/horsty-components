// Generated with util/create-component.js
import React from "react";

import { CardProps } from "./Card.types";

import "./Card.scss";
import SvgOrnement from "../Icons/Ornement";

const hexaColor = getComputedStyle(document.documentElement).getPropertyValue(
  `--orange`
);
const Card: React.FC<CardProps> = ({ title, content, footer }) => (
  <div className="card" data-testid="test-card">
    <div className="header">
      <SvgOrnement className="placeholderIcon" fill={hexaColor} />
      <h2>{title}</h2>
      <SvgOrnement className="placeholderIcon" fill={hexaColor} />
    </div>
    <div className="content app-padding-left-xl">{content}</div>
    <div className="footer">{footer}</div>
  </div>
);

export default Card;
