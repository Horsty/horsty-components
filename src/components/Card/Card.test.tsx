// Generated with util/create-component.js
import React from "react";
import { render } from "@testing-library/react";

import Card from "./Card";
import { CardProps } from "./Card.types";

describe("Test Component", () => {
  let props: CardProps;

  beforeEach(() => {
    props = {
      title: "Rechercher une carte", 
      content: "ceci est un content", 
      footer: "ceci est un footer"
    };
  });

  const renderComponent = () => render(<Card {...props} />);

  it("should render foo text correctly", () => {
    props.title = "Rechercher une carte";
    const { getByTestId } = renderComponent();

    const component = getByTestId("test-card");

    expect(component).toHaveTextContent("ceci est un content");
  });
});
