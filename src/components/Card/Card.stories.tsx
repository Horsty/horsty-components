import React from "react";
import Card from "./Card";
import TextInput from "../Input/Input";
import Button from "../Button/Button";
import { action } from "@storybook/addon-actions";

export default {
  title: "Card",
  component: Card,
};

const primaryButton = {
  text: "Selected",
  type: "primary",
};
const actions = {
  onClick: action("onClick"),
};
/**
 * TextInput
 */
const input = (state, setState) => (
  <TextInput
    label="Nom de la carte"
    name="name"
    placeholder="Entrez ici le nom de la carte"
    value={state.value}
    onChange={(e) => setState({ value: e.target.value })}
  />
);

const button = (
  <Button text={primaryButton.text} type={primaryButton.type} {...actions} />
);

export const WithInputAndButton = (state, setState) => (
  <Card
    title="Rechercher une carte"
    content={input(state, setState)}
    footer={button}
  ></Card>
);

// export const WithBaz = () => <Card input="baz" />;
