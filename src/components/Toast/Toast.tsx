import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

import "./Toast.scss";

import { ToastProps } from "./Toast.types";
import SvgCross from "../Icons/Cross";
import SvgCheck from "../Icons/Check";
import SvgWarning from "../Icons/Warning";
import SvgInfo from "../Icons/Info";
import SvgError from "../Icons/Error";

const Toast: React.FC<ToastProps> = (props) => {
  const { toastList, position, autoDelete, dismissTime } = props;
  const [list, setList] = useState(toastList);

  useEffect(() => {
    setList([...toastList]);

    // eslint-disable-next-line
  }, [toastList]);

  useEffect(() => {
    const interval = setInterval(() => {
      if (autoDelete && toastList.length && list.length) {
        deleteToast(toastList[0].id);
      }
    }, dismissTime);

    return () => {
      clearInterval(interval);
    };

    // eslint-disable-next-line
  }, [toastList, autoDelete, dismissTime, list]);

  const deleteToast = (id) => {
    const listItemIndex = list.findIndex((e) => e.id === id);
    const toastListItem = toastList.findIndex((e) => e.id === id);
    list.splice(listItemIndex, 1);
    toastList.splice(toastListItem, 1);
    setList([...list]);
  };
  const renderSwitch = (icon) => {
    switch (icon) {
      case "check":
        return <SvgCheck />;
      case "error":
        return <SvgError />;
      case "warning":
        return <SvgWarning />;
      case "info":
      default:
        return <SvgInfo />;
    }
  };

  return (
    <>
      <div className={`notification-container ${position}`}>
        {list.map((toast, i) => (
          <div
            key={i}
            className={`notification toast ${position}`}
            style={{
              backgroundColor: getComputedStyle(
                document.documentElement
              ).getPropertyValue(`--${toast.backgroundColor}`),
            }}
          >
            <div className="notification-cross">
              <SvgCross onClick={() => deleteToast(toast.id)} />
            </div>
            <div className="notification-image">{renderSwitch(toast.icon)}</div>
            <div>
              <p className="notification-title">{toast.title}</p>
              <p className="notification-message">{toast.description}</p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

Toast.propTypes = {
  toastList: PropTypes.array.isRequired,
  position: PropTypes.string,
  autoDelete: PropTypes.bool,
  dismissTime: PropTypes.number,
};

export default Toast;
