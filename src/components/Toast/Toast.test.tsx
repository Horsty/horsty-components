import React from "react";
import { render } from "@testing-library/react";

import Toast from "./Toast";
import { ToastProps } from "./Toast.types";

describe("Test Component", () => {
  let props: ToastProps;

  beforeEach(() => {
    props = {
      text: `You've assigned Owner of Button Component`,
      type: "info",
      hideClose: false,
    };
  });

  const renderComponent = () => render(<Toast {...props} />);

  it("should have toast--info className with default props", () => {
    const { getByTestId } = renderComponent();

    const Toast = getByTestId("test-toast");

    expect(Toast).toHaveClass("toast--info");
  });

  it("should have toast--alert className with type set as alert", () => {
    props.type = "alert";
    const { getByTestId } = renderComponent();

    const Toast = getByTestId("test-toast");

    expect(Toast).toHaveClass("toast--alert");
  });
});
