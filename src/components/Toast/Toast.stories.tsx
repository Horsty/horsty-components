import React from "react";
import { action } from "@storybook/addon-actions";
import Toast from "./Toast";

export default {
  title: "Toast",
  component: Toast,
};
const actions = {
  onClick: action("onClick"),
};
const infoToast = {
  title: 'Info',
  description: 'This is an info toast component',
  backgroundColor: 'blue',
  icon: 'info'
}
const errorToast = {
  title: 'Error',
  description: 'This is an error toast component',
  backgroundColor: 'red',
  icon: 'error'
}
const checkToast = {
  title: 'Check',
  description: 'This is an check toast component',
  backgroundColor: 'green',
  icon: 'check'
}
const warningToast = {
  title: 'Warning',
  description: 'This is an warning toast component',
  backgroundColor: 'orange',
  icon: 'warning'
}
/**
 * Primary button
 */
export const info = () => (
  <Toast 
  toastList={[infoToast]}
  position={'top-right'}
  autoDelete={false}
  dismissTime={0}
/>
);
/**
 * Primary button
 */
export const warning = () => (
  <Toast 
  toastList={[warningToast]}
  position={'top-right'}
  autoDelete={false}
  dismissTime={0}
/>
);
/**
 * Primary button
 */
export const check = () => (
  <Toast 
  toastList={[checkToast]}
  position={'top-right'}
  autoDelete={false}
  dismissTime={0}
/>
);
/**
 * Primary button
 */
export const error = () => (
  <Toast 
  toastList={[errorToast]}
  position={'top-right'}
  autoDelete={false}
  dismissTime={0}
/>
);