export interface ToastProps {
  text?: string;
  type?: string;
  onClick?: any;
  hideClose?: boolean;
  
  toastList: any;
  position?: string;
  autoDelete?: boolean;
  dismissTime?: number;
}
